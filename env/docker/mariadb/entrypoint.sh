echo "> Setting up bind address to accept connections from outside the container..."
sed -i "s@bind\-address\(.*\)@bind-address = 0\.0\.0\.0@g" /etc/mysql/my.cnf

chown -R mysql:mysql /var/lib/mysql

echo "> Starting MariaDB server..."
service mysql start

if [ "${DATABASE}" != "" ] && [ ! -d "/var/lib/mysql/${DATABASE}" ] ; then
    echo "> Creating database '${DATABASE}'..."
    echo "CREATE DATABASE ${DATABASE};" | mysql

    echo "> Grant privileges to user '${USERNAME}' on '${DATABASE}' database..."
    echo "GRANT all ON *.* TO '${USERNAME}'@'%' IDENTIFIED BY '${PASSWORD}' WITH GRANT OPTION; FLUSH PRIVILEGES" | mysql
    echo "GRANT SUPER ON *.* TO '${USERNAME}'@'%' IDENTIFIED BY '${PASSWORD}' WITH GRANT OPTION; FLUSH PRIVILEGES" | mysql

    if [ "${DUMPFILE}" != "" ] && [ -f "/var/tmp/mysql/${DUMPFILE}" ] ; then
        echo "> Importing database..."
        mysql ${DATABASE} < /var/tmp/mysql/${DUMPFILE}
    fi
fi

echo "> Reading MariaDB errors log (/var/log/mysql/error.log)..."
tail -f /var/log/mysql.err
