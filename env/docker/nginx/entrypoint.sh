# Nginx server.
if [ "${HOST}" != "" ] ; then
    echo "> Replacing host variable in /etc/nginx/sites-available/default..."
    sed -i "s|{{host}}|${HOST}|g" /etc/nginx/sites-available/default
fi

if [ "${ROOT}" != "" ] ; then
    echo "> Replacing root folder variable in /etc/nginx/sites-available/default..."
    sed -i "s|{{root}}|${ROOT}|g" /etc/nginx/sites-available/default
fi

if [ "${BACKEND}" != "" ] ; then
    echo "> Replacing PHP host variable in /etc/nginx/sites-available/default..."
    sed -i "s|{{backend}}|${BACKEND}|g" /etc/nginx/sites-available/default
fi

if [ "${MODE}" != "" ] ; then
    echo "> Replacing Magento Mode variable in /etc/nginx/sites-available/default..."
    sed -i "s|{{mode}}|${MODE}|g" /etc/nginx/sites-available/default
fi

if [ "${SUBDOMAINS}" != "" ] ; then
    echo "> Replacing Subdomain variable in /etc/nginx/sites-available/default..."
    sed -i "s|{{subdomains}}|${SUBDOMAINS}|g" /etc/nginx/sites-available/default
fi

echo "> Starting Nginx..."
service nginx start

echo "> Reading Nginx errors log (/var/log/nginx/error.log)..."
tail -f /var/log/nginx/error.log
