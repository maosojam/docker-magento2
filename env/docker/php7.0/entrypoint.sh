# PHP7-FPM.

echo "> Setting and starting Cron..."
#echo "* * * * * root php ${ROOT}/update/cron.php"            >> /etc/crontab
#echo "* * * * * root php ${ROOT}/bin/magento setup:cron:run" >> /etc/crontab
#echo "* * * * * root php ${ROOT}/bin/magento cron:run"       >> /etc/crontab
#cron -f &

echo "> Starting PHP7..."
service php7.0-fpm start

#echo "> Setting up Magento..."
#php ${ROOT}/bin/magento module:enable --all
#php ${ROOT}/bin/magento setup:di:compile
#php ${ROOT}/bin/magento setup:upgrade
#php ${ROOT}/bin/magento setup:static-content:deploy
#php ${ROOT}/bin/magento setup:static-content:deploy -l es_AR

#echo "> Setting up folders permission..."
#chmod 777 ${ROOT}/app/etc -R
#chmod 777 ${ROOT}/vendor -R
#chmod 777 ${ROOT}/var -R
#chmod 777 ${ROOT}/pub -R
echo "> Configuring Postfix..."
echo [${SMTP}]:${SMTP_PORT} ${MAIL}:${MAIL_PASSWORD} > /etc/postfix/sasl_passwd
chmod 400 /etc/postfix/sasl_passwd
postmap /etc/postfix/sasl_passwd

service postfix start

echo "> Reading PHP7 errors log (/var/log/php7.0-fpm.log)..."
tail -f /var/log/php7.0-fpm.log
