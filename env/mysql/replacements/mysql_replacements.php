<?php

date_default_timezone_set('America/Argentina/Buenos_Aires');

// error_reporting(E_ALL | E_STRICT);
// ini_set('display_errors', 1);

/**
 * valores globales
 */

const SCRIPT_VERSION = '1.1';

// bool: si se llamo con la opcion -q
$_run_quiet = false;

// bool: si se llamo con la opcion -v
$_verbose = false;

// bool: si se llamo con la opcion -t
$_test_only = false;

// bool: si se llamo con la opcion -h
$_help = null;

// bool: si se llamo con la opcion -f
$_use_conf_file = false;



// string: parametro de entrada de archivo de origen
$_origin_file = null;

// string: parametro de entrada de archivo de origen
$_dest_file = null;

// mensaje de error para salida por fellos
$_err_msg = null;

//estructura de configuracion
$_conf;

//punteros de archivos de entrada y salida
$_of = null;
$_df = null;

//numero de linea actual en proceso
$_ln_num = 0;

/**
 * sale del script con error
 */
function exit_error() {
    global $_err_msg, $_run_quiet, $_of, $_df;
    if (!$_run_quiet) {
        echo "\n ERROR : $_err_msg \n";
        print_help_message();
    }

    if ($_of)
        fclose($_of);
    if ($_df)
        fclose($_df);
    exit(1);
}

function echo_log($msg){
    global $_verbose;

    if( $_verbose)
        echo date('H:i:s').' '.$msg."\n";
}

/**
 * Imprime el mensaje de ayuda
 */
function print_help_message() {
    $hlp = "

 DSG Solutions Mysql Replacements (Version: " . SCRIPT_VERSION . ")
 Modo de uso:
 
 php mysql_replacements.php [OPCIONES] origin_file dest_file json_conf
 
PARAMETROS
 
 origin_file : mysql dump de origen 
 dest_file : mysql dump de destino
 json_config : estructura json de configuracion (con la opcion -f)
 
OPCIONES:

 -h Help: muestra esta ayuda
 -q Quiet : no muestra informacion por salida standard
 -v Verbose: muestra informacion adicional del proceso
 -t Test Only: no genera el archivo de salida, solo para testing
 -f File Conf: el parametro json_config se leera  desde un archivo json (tercer parametro)
    
";
    echo $hlp;
}

/**
 * evalua las opciones (-q y -h)
 */
function eval_option($option) {
    global $_run_quiet, $_help, $_verbose, $_test_only, $_use_conf_file;

    if ($option == 'q')
        $_run_quiet = true;

    if ($option == 'h') {
        $_help = true;
    }

    if ($option == 'v') {
        $_verbose = true;
        echo_log('Modo Verbose activado');
    }

    if ($option == 't') {
        $_test_only = true;
        echo_log('Modo Test Only activado, no se generarÃ¡ archivo de salida');
    }

    if ($option == 'f') {
        $_use_conf_file = true;
        echo_log('Modo File se leera la configuracion de un archivo json');
    }
}

/**
 * Valida que exista el archivo de origen
 */
function validate_origin_file() {
    global $_err_msg, $_origin_file;
    if (!file_exists($_origin_file)) {
        $_err_msg = ' No existe el archivo de origen: ' . $_origin_file;
        return false;
    }
    return true;
}

/**
 * Valida que exista y sea vÃ¡lido el archivo de configuracion
 * @return boolean
 */
function validate_conf_file() {

    global $_err_msg, $_conf_file, $_conf, $_use_conf_file;

    if($_use_conf_file){

        if (!file_exists($_conf_file)) {
            $_err_msg = ' No existe el archivo de configuracion : ' . $_conf_file;
            return false;
        }

        echo_log(" Leyendo archivo de configuracion: $_conf_file");
        $_conf = file_get_contents($_conf_file);


        //quita los comentarios
        $_conf = preg_replace('/(^|[^:])\/\/.*\n/', '', $_conf);

    } else {
        $_conf = $_conf_file;
    }

    $_conf = json_decode($_conf);
    if ($_conf === null) {
        if ($_use_conf_file)
            $_err_msg = ' No se pudo decodificar correctamente archivo de configuracion : ' . $_conf_file;
        else
            $_err_msg = ' No se pudo decodificar correctamente la estructura de configuracion : ';
        return false;
    }

    //todo: test regular expr., validacion de opciones obligatorias
    return true;
}

/**
 * Valida que se pueda crear el archivo de salida
 * @return boolean
 */
function validate_dest_file() {
    global $_err_msg, $_dest_file, $_test_only;

    if ($_test_only)
        return true;

    if (file_put_contents($_dest_file, " -- " . date('d/m/Y H:i:s') . " - DSG Solutions Mysql Replacements (Version: " . SCRIPT_VERSION . ") \n") === false) {
        $_err_msg = ' No se puede crear el archivo de destino : ' . $_dest_file;
        return false;
    }
    return true;
}

/**
 * Parseo de parÃ¡metros de entrada
 */
function parse_inputs() {
    global $argv, $_origin_file, $_dest_file, $_conf_file, $_err_msg, $_help;

    $num = 1;
    foreach ($argv as $i => $value) {

        if ($i == 0)
            continue; //omitimos el primer parÃ¡metro

        if (preg_match('/^\-[a-z]/', $value)) { //si es una opcion
            $option = preg_replace('/[^a-z]/', '', $value);
            eval_option($option);
        } else {
            if ($num == 1) {
                $_origin_file = $value;
            }
            if ($num == 2) {
                $_dest_file = $value;
            }
            if ($num == 3) {
                $_conf_file = $value;
            }
            $num++;
        }
    }

    //opcion -h
    if ($_help) {
        return true;
    }

    // echo "\n\n";
    // echo($_origin_file);
    // echo "\n\n";
    // echo($_dest_file);
    // echo "\n\n";
    // echo($_conf_file);
    // echo "\n\n";
    // die('TU VIEJA');

    if (!$_origin_file || !$_dest_file || !$_conf_file) {
        $_err_msg = ' ParÃ¡metros insuficientes ';
        return false;
    }

    if (!validate_origin_file()) {
        return false;
    }

    if (!validate_dest_file()) {
        return false;
    }

    if (!validate_conf_file()) {
        return false;
    }

    return true;
}

/**
 * Apertura de archivos de entrada y salida
 * @return boolean
 */
function open_files() {
    global $_of, $_df, $_origin_file, $_dest_file, $_err_msg, $_test_only;

    $_of = fopen($_origin_file, 'r');
    if ($_of == false) {
        $_err_msg = ' No se pudo abrir el archivo de origen';
        return false;
    }

    if ($_test_only)
        return true;

    $_df = fopen($_dest_file, 'a');

    if ($_of == false) {
        $_err_msg = ' No se pudo abrir el archivo de destino';
        return false;
    }

    return true;
}

/**
 * Devuele la siguiente linea del archivo de entrada
 * @return string
 */
function next_line() {
    global $_of, $_err_msg, $_ln_num;
    $line = fgets($_of);

    if ($line === false && !feof($_of)) {
        $_err_msg = ' No se pudo traer una linea ' . ($_ln_num + 1) . ' del archivo de entrada ';
        exit_error();
    }

    $_ln_num++;

    return $line;
}

/**
 * Graba la linea en el archivo de salida
 * @param type $line
 * @return boolean
 */
function add_line($line) {
    global $_df, $_err_msg, $_ln_num, $_test_only;

    if ($_test_only)
        return true;

    if (!fwrite($_df,$line)) {
        $_err_msg = ' No se pudo escribir la linea ' . ($_ln_num + 1) . ' en el archivo de salida ';
        exit_error();
    }
    return true;
}

// estructura temporal de configuraciones de tablas, se va generando a medida
// que se lee el archivo de entrada
$_tables = Array();

/**
 * Devuelve true si se debe omitir la inserciÃ³n de datos para la tabla $table_name
 * @param string $table_name nombre de la tabla que se esta procesando
 * @return boolean
 */
function skip_table($table_name) {
    global $_tables, $_conf;

    if (!isset($_tables[$table_name]) || !isset($_tables[$table_name]['skip'])) {
        $_tables[$table_name] = Array('skip' => false);

        if (isset($_conf->skip_tables))
            foreach ($_conf->skip_tables as $sk) {
                if (preg_match($sk, $table_name))
                    $_tables[$table_name]['skip'] = true;
            }
    }
    return $_tables[$table_name]['skip'];
}

//global para skip de replace mails
$_skip_replace_mails = null;


/**
 * Devuelve true si se debe reemplazar los mails para la tabla $table_name
 * @param string $table_name nombre de la tabla que se esta procesando
 * @return boolean
 */
function replace_emails($table_name) {
    global $_tables, $_conf, $_skip_replace_mails;

    if ($_skip_replace_mails === null ){


        //carga la conf de $_skip_replace_mails por unica vez
        $_skip_replace_mails = false;
        if (isset($_conf->replace_emails) && isset($_conf->replace_emails->skip)){
            $_skip_replace_mails = $_conf->replace_emails->skip;
        }
    }

    if (!isset($_tables[$table_name]) || !isset($_tables[$table_name]['replace_emails'])) {
        $_tables[$table_name] = Array('replace_emails' => false);

        //reemplazo para todas las tablas
        if (isset($_conf->replace_emails) && ! isset($_conf->replace_emails->tables)){
            $_tables[$table_name]['replace_emails'] = true;
            return true;
        }

        if (isset($_conf->replace_emails))
            foreach ($_conf->replace_emails->tables as $sk) {
                //echo "probando $sk vs $table_name \n";
                if (preg_match($sk, $table_name))
                    $_tables[$table_name]['replace_emails'] = true;
            }
    }
    return $_tables[$table_name]['replace_emails'];
}

/**
 * Devuelve un array asociativo after/before con los array de expresiones regulares de reemplazo de urls
 *  en la tabla  $table_name, false si no se deben aplicar estos reemplazaos
 * @param string $table_name nombre de la tabla que se esta procesando
 * @return array
 */
function replace_urls($table_name) {
    global $_tables, $_conf,$_urls_replacements_after;

    if (!isset($_tables[$table_name]) || !isset($_tables[$table_name]['replace_urls'])) {
        $_tables[$table_name] = Array('replace_urls' => false);

        if(!isset($_conf->replace_urls))
            return false;

        //si no esta definido el array de tablas para replace_urls, supongo que se reemplazan en todas las tablas
        if (!isset($_conf->replace_urls->tables)) {
            $_tables[$table_name]['replace_urls'] = true;
        }
        else
            foreach ($_conf->replace_urls->tables as $sk) {
                if (preg_match($sk, $table_name))
                    $_tables[$table_name]['replace_urls'] = true;
            }

        //si esta definido el reemplazo de urls para estas tablas armo el array de expresiones regulares
        if ($_tables[$table_name]['replace_urls']) {

            $_tables[$table_name]['replace_urls'] = Array("before" => array(), "after" => Array());
            foreach ($_conf->replace_urls->urls as $url) {
                //s:[0-9]+:\\\"http:\/\/www\.e-farmacity\.com[^;]*\\\";

                $_tables[$table_name]['replace_urls']['before'][] = '/s:[0-9]+:\\\"'.preg_quote($url->before, '/').'([^;]*)\\\";/';
                //$_tables[$table_name]['replace_urls']['after'][] = '/s:'.(strlen($url->after) + strlen('$1')).':\"'.$url->after.'$1\";';
                $_tables[$table_name]['replace_urls']['after'][] = $url->after;

//                $_tables[$table_name]['replace_urls']['callback']['/s:[0-9]+:\\\"'.preg_quote($url->before, '/').'([^;]*)\\\";/'] = $url->after;

                $_tables[$table_name]['replace_urls']['before'][] = '/' . preg_quote($url->before, '/') . '/';
                $_tables[$table_name]['replace_urls']['after'][] = $url->after;


                //echo "probando $sk vs $table_name \n";
            }
        }
    }
    // $_urls_replacements_after = null;
//    if($_tables[$table_name]['replace_urls'])
//        $_urls_replacements_after = $_tables[$table_name]['replace_urls']['callback'];

    return $_tables[$table_name]['replace_urls'];
}


//globales para la funcion de callback de urls
$_urls_replacements_after = null;
$_urls_replacements_before = null;


function callback_urls($s){
    global $_urls_replacements_after, $_urls_replacements_before;

    $after = $_urls_replacements_after;
    if(substr($_urls_replacements_before,0,10) == '/s:[0-9]+:' )
        $after = '/s:'.(strlen($after) + strlen($s[1]) ).':\"'.$after.$s[1].'\";';
    return $after;
}


function callback_emails($s){
    global  $_skip_replace_mails;

    if($_skip_replace_mails  && preg_match($_skip_replace_mails, $s[0]) ){
        return $s[1];
    }

    if (substr($s[0], 0, 2) == 's:') //email en serializado
        $res = 's:'.strlen(md5($s[1])."@test.com").':\"'.md5($s[1])."@test.com".'\";';
    else
        $res = md5($s[1])."@test.com";
    return $res;
}

/**
 * Devuelve un array asociativo after/before con los array de expresiones regularesr de remplazo custom
 *  en la tabla  $table_name, false si no se deben aplicar reemplazaos
 * @param string $table_name nombre de la tabla que se esta procesando
 * @return array
 */
function replace_custom($table_name) {
    global $_tables, $_conf;

    if (!isset($_tables[$table_name]) || !isset($_tables[$table_name]['replace_custom'])) {
        $_tables[$table_name] = Array('replace_custom' => Array("before" => array(), "after" => Array()));

        if(!isset($_conf->replace_custom))
            return false;

        foreach ($_conf->replace_custom as $custom) {
            if (preg_match($custom->table, $table_name)) {
                $_tables[$table_name]['replace_custom']['before'][] = $custom->before;
                $_tables[$table_name]['replace_custom']['after'][] = $custom->after;
            }
        }

        //si no aplica ningun remplazo custom para la tabla
        if (empty($_tables[$table_name]['replace_custom']['before'])) {
            $_tables[$table_name]['replace_custom'] = false;
        }
    }
    return $_tables[$table_name]['replace_custom'];
}

/**
 * Devuelve un array asociativo after/before con los array de expresiones regulares de reemplazo passwords
 *  en la tabla  'admin_user'
 * @param string $table_name nombre de la tabla que se esta procesando
 * @return array
 */
function replace_passwords() {
    global $_tables, $_conf;
    $table_name = 'admin_user';
    if (!isset($_tables[$table_name]) || !isset($_tables[$table_name]['replace_password'])) {
        $_tables[$table_name] = Array('replace_password' => Array("before" => array(), "after" => Array()));

        if(!isset($_conf->reset_password))
            return false;

        foreach ($_conf->reset_password as $custom) {
            $_tables[$table_name]['replace_password']['before'][] = "/,'" . $custom->username . "','.*?','(.*?)','(.*?)','(.*?)',(.*?),(.*?),(.*?),/";
            $_tables[$table_name]['replace_password']['after'][] =   ",'" . $custom->username . "','" . md5('qX' . $custom->new_password) .':qX'."','$1','$2','$3',$4,$5,1,";
        }
    }

    return $_tables[$table_name]['replace_password'];
}


/**
 * Realiza los reempalzos necesarios sobre una linea del archivo en base a la configuracion
 * @param string $line
 */
function process_line($line) {
    global $_conf,$_tables,$_urls_replacements_after, $_urls_replacements_before;
    //skip insert into table
    $matches = Array();

    if (preg_match('/^INSERT INTO \`([^\`]*)\` VALUES /', $line, $matches)) {

        $table_name = $matches[1];

        if (!isset($_tables[$table_name]))
            echo_log("Tabla: ".$table_name);

        //skip tables
        if (skip_table($table_name)) {
            echo_log ("Salteando: $table_name");
            return;
        }


        //replace mails
        if (replace_emails($table_name)) {
            $count_tot = 0;

            $count = 0;

            // si el mail esta dentro de un serializado
            $line = preg_replace_callback('/s:[0-9]+:\\\"([-.a-zA-Z0-9_+]+@[-.a-zA-Z0-9]+)\\\";/', 'callback_emails', $line, -1, $count);
            $count_tot += $count;

            $count = 0;
            $line = preg_replace_callback('/([-.a-zA-Z0-9_+]+@[-.a-zA-Z0-9]+)/', 'callback_emails', $line, -1, $count);
            $count_tot += $count;

            if($count_tot){
                echo_log("  $count_tot mails reemplazados");
            }
        }

        //replace urls
        $url_replaces = replace_urls($table_name);
        if ($url_replaces !== false) {
            //todo: si esta dentro de un serializado: ej: s:[0-9]+:\\\"http:\/\/www\.e-farmacity\.com[^;]*\\\";
            //$line = preg_replace($url_replaces['before'], $url_replaces['after'], $line);

            for($i= 0 ; $i< count($url_replaces['before']); $i++){
                //globales para la funcion de callback de urls
                $_urls_replacements_after = $url_replaces['after'][$i];
                $_urls_replacements_before= $url_replaces['before'][$i];
                $count=0;
                $line = preg_replace_callback($url_replaces['before'][$i], 'callback_urls', $line, -1, $count);

                if($count)
                    echo_log("  $count urls reemplazadas");
            }

        }

        //replace_custom
        $custom_replaces = replace_custom($table_name);
        if ($custom_replaces !== false) {

            $line = preg_replace($custom_replaces['before'], $custom_replaces['after'], $line);
        }

        //reset password
        if ($table_name == 'admin_user' && isset($_conf->reset_password)) {
            $pass_replaces = replace_passwords();
            $line = preg_replace($pass_replaces['before'], $pass_replaces['after'], $line);
        }
    } //replace definer
    else if (isset($_conf->replace_definer) && preg_match("/^\/\*\!.*DEFINER=" . $_conf->replace_definer->before . "/", $line)) {//.*DEFINER=`".isset($_conf->replace_definer->before)."`@`.*?`/", $line)){
        $line = preg_replace("/DEFINER=" . $_conf->replace_definer->before . "/", "DEFINER=" . $_conf->replace_definer->after , $line);
    }

    //se agrega la linea al archivo de salida
    add_line($line);

}

////////////////////////////////////////////////////////////////////////////////
//MAIN

$_ini_time = time();

echo_log("Leyendo parametros y configuracion configuracion");

if (!parse_inputs()) {
    exit_error();
}

// opcion -h
if ($_help) {
    if (!$_run_quiet) {
        print_help_message();
    }
    return 0;
}

echo_log("Abriendo archivos ");

if (!open_files()) {
    exit_error();
}


while ($line = next_line()) {
    process_line($line);
}

//exit success
if (!$_run_quiet) {
    echo date('H:i:s')." Se proceso el archivo correctamente, $_ln_num lineas," . count(array_keys($_tables)) . " tablas en " . (time() - $_ini_time) . " segundos \n";
}

exit(0);