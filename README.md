# docker-magento2

# Magento 2 - Entorno local de desarrollo con Docker

### @author     Maximiliano Osorio <osoriomao@gmail.com>
### @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)

### Utilizando Docker se automatizada la instalación y configuración para crear un entorno completamente funcional de Magento 2.
### Probado sobre un sistema Linux (Ubuntu 16.04 LTS).
### Infraestructura del entorno:
#### Nginx + PHP 7.0 + MariaDB

## 1. Encontrará la siguiente estructura de directorios
### src/ (aquí debe estar el código de Magento 2)
### env/ (archivos de configuración para la creación del entorno)
	#### docker/
		##### docker-compose-linux.yml (reemplazar dentro de este archivo el texto "NOMBRE_ENTORNO" por el nombre del entorno)
	#### magento/
		##### env.php.local (reemplazar "NOMBRE_ENTORNO" por el mismo nombre utilizado en el archivo .yml)
	#### mysql/
		##### NOMBRE_ENTORNO.sql (un dump de la DB)

#### Importante "NOMBRE_ENTORNO" se debe reemplazar por el nombre del nuevo entorno sin espacios, sin caractéres especial y en minúscula.

## 2. Copiar env.php
Debemos copiar el archivo env/magento/env.php.local a copiar a src/app/etc/env.php

## 3. Ejecutar los siguientes comandos (dentro del directorio env/docker)

### sudo docker-compose -f docker-compose-linux.yml build 
Genera las imagenes que va a utilizar docker para el entorno.

### sudo docker-compose -f docker-compose-linux.yml up -d
Inicia los contenedores, similar a una máquina virtual.

## 4. Comandos útiles

### sudo docker ps -a 
Muestra el estado de los contenedores, si están iniciados.

### sudo docker inspect NOMBRE_ENTORNO-webserver | grep IPAddress
Con este comando podemos ver la ip asignada al contenedor.

### sudo docker exec -it NOMBRE_ENTORNO-backend /bin/bash
Nos permite acceder vía SSH al contenedor. El contenedor NOMBRE_ENTORNO-backend, es donde está el código, dentro del directorio /var/www veremos lo mismo que tenemos en el directorio src/.

#### composer
(será necesario realizar un "composer install / composer update" para descargar el directorio "vendor" de Magento)

## 5. Configurar archivo "hosts"
##### Será necesario agregar en el archivos "hosts" en la máquina Host una nueva línea
Ejemplo:
127.0.0.1 www.NOMBRE_ENTORNO.local NOMBRE_ENTORNO.local

Según las configuraciones del entorno puede ser necesario modificar algunos parámetros extras en la tabla "core_config_data" en base de datos.

### 6. Conectar a la DB 
#### Es posible utilizar cualquier herramienta visual que funcione con MySQL, con los siguiente datos:
##### Host: NOMBRE_ENTORNO-database
##### Puerto: 3306 (estándar)
##### Nombre DB: NOMBRE_ENTORNO
##### Contraseña DB: NOMBRE_ENTORNO

#### Modificar en la DB la tabla core_config_data con el dominio local
##### update core_config_data set value = 'http://NOMBRE_ENTORNO.local/' where path = 'web/unsecure/base_url';
##### update core_config_data set value = 'http://NOMBRE_ENTORNO.local/' where path = 'web/secure/base_url';

#### RECOMENDADO - Sanitizar DB, se modifican los correos de clientes con valores aleatorios
##### UPDATE customer_entity SET email = concat(md5(rand()),"@test.com");

#### Sugerencias en caso de encontrar errores (por ejemplo: ERR_TOO_MANY_REDIRECTS), revisar:
##### select * from core_config_data where path = 'admin/url/custom';
##### select * from core_config_data where path like '%url%';
##### select * from core_config_data where path like '%cookie%';

### 7 . Consideraciones generales

#### Interfáz de usuario: NOMBRE_ENTORNO.local/

#### Panel de administación: NOMBRE_ENTORNO.local/panel

#### Permisos de Magento
Desde el sistema guest (Ubuntu) ejecutar el siguente comando:
sudo chown -R magento:magento src/


#### Iniciar y detener los contenedores de Docker
Dentro del directorio env/docker/ ejecutar:

##### sudo docker-compose -f docker-compose-linux.yml start
##### sudo docker-compose -f docker-compose-linux.yml stop (libera los recursos)
